# uStats

An [UBports](https://ubports.com) app to show data statistics on your device using vnstat and vmstat.

#### !!!Warning, at the moment this app runs unconfined and therefore may harm your system if not used properly!!!

## Prerequisites
Using vmstat works out of the box for that comes shipped with upports.
But the package **vnstat** needs to be installed on the device and set up in a working configuration.
- Download vnstat for armhf [debian packages](https://packages.debian.org/sid/armhf/vnstat/download) (alternatively fix build for armhf of the package in the repo and use that)
- Make your system writable with [UT Tweak Tool](https://open-store.io/app/ut-tweak-tool.sverzegnassi) (set __read/write until next reboot__)
- Open Terminal and install vnstat

  __sudo dpkg -i PATHANDNAMEOFPACKAGE__

- configure vnstat (credits go to [ubuntuusers.de](https://wiki.ubuntuusers.de/vnStat/))
  - get the name of your network controller using

    **netstat -i** to list available controllers

    **route -n** to show controllers in use
  - edit the default controller in the file __/etc/vnstat.conf__ to your controller i.e.

    \# default interface

    Interface "ccmni0"

  - create a database that hold vnstat's data, make sure you place it in a folder where you do have read/write access i.e. /home/phablet/Documents/vnstat

    __sudo vnstat -u -i INTERFACE__ with INTERFACE is your controller i.e.

    __sudo vnstat -u -i /home/phablet/Documents/vnstat/ccmni0__

  - change the access for the database with

    __sudo chown -R vnstat:vnstat PATH_TO_YOUR_DATABASE__ , for our example that would be:

    __sudo chown -R vnstat:vnstat /home/phablet/Documents/vnstat__

  - start vnstat server so it collects data with

    __sudo service vnstat start__

  - wait a couple of minutes so vnstat can collect some data, now you are ready to go, run

     __vnstat__ use --months to get monthly output or --days for daily output
