import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

Page {
    title: i18n.tr("About")

    header: PageHeader {
        id: header
        title: parent.title
    }

    Flickable {
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        contentHeight: contentColumn.height + units.gu(4)

        ColumnLayout {
            id: contentColumn
            anchors {
                left: parent.left;
                top: parent.top;
                right: parent.right;
                margins: units.gu(2)
            }
            spacing: units.gu(2)

            Label {
                Layout.fillWidth: true
                text: "uStats v%1".arg("0.2.0")
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottomMargin: units.gu(4)
                textSize: Label.Large
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            UbuntuShape {
                Layout.preferredHeight: units.gu(10)
                Layout.preferredWidth: units.gu(10)
                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter

                source:  Image {
                    source: '../assets/logo.svg'
                }
            }

            Label {
                Layout.fillWidth: true

                text: i18n.tr("This app is using vnstat and vmstat for armhf.");
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            Label {
                Layout.fillWidth: true

                text: i18n.tr("An app for your UBports mobile linux device. Consider donating if you like UBports and want to support this free OS it!")
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            Button {
                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter

                text: i18n.tr("Donate to UBports") + " ➦"
                color: UbuntuColors.orange
                onClicked: Qt.openUrlExternally('https://ubports.com/en_EN/donate')
            }
        }
    }
}
