import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Shellexec 1.0

Page {
    title: i18n.tr("vmstat memory statistics")

    header: PageHeader {
        id: header
        title: parent.title

        trailingActionBar.actions: [
            Action {
                iconName: "edit-clear"
                text: i18n.tr("Quit")
                onTriggered: {
                    Qt.quit()
                }
            },
            Action {
                iconName: "reload"
                text: i18n.tr("Reload data")
                onTriggered: {
                  memtotal.text = parseVmstatOutput(0)
                  memused.text = parseVmstatOutput(1)
                  memfree.text = parseVmstatOutput(4)
                  buffermem.text = parseVmstatOutput(5)
                  swapcache.text = parseVmstatOutput(6)
                  cachetotal.text = parseVmstatOutput(7)
                  cacheused.text = parseVmstatOutput(8)
                  cachefree.text = parseVmstatOutput(9)
                  memactive.text = parseVmstatOutput(2)
                  meminactive.text = parseVmstatOutput(3)
                }
            }
        ]
    }

    Shellexec {
      id: launchervmstat
    }

    function parseVmstatOutput(lineIndex) {
      //read output of command 'vmstat -stats'
      var output = launchervmstat.launch("vmstat -stats").split("\n");
      var value = output[lineIndex].substr(0,13).replace(" ","");
      return value
    }

    Flickable {
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        contentHeight: datagrid.height + units.gu(4)

        GridLayout {
            id: datagrid
            columns: 3

            anchors {
                    left: parent.left;
                    top: parent.top;
                    right: parent.right;
                    margins: units.gu(2)
                }

            Text {
              text: i18n.tr("Selected values of") + " \'vmstat -stats\' :";
              Layout.columnSpan: 3;
              font.bold: true;
              color: theme.palette.normal.baseText;
            }

            Text { text: i18n.tr("total memory"); color: theme.palette.normal.foregroundText;}
            Text {
              id: memtotal;
              text: parseVmstatOutput(0);
              color: theme.palette.normal.foregroundText;
            }
            Text { text: "K"; color: theme.palette.normal.foregroundText;}

            Text { text: i18n.tr("used memory");color: theme.palette.normal.foregroundText;}
            Text {
              id: memused;
              text: parseVmstatOutput(1);
              color: theme.palette.normal.foregroundText;
            }
            Text { text: "K"; color: theme.palette.normal.foregroundText;}

            Text { text: i18n.tr("free memory");color: theme.palette.normal.foregroundText;}
            Text {
              id: memfree;
              text: parseVmstatOutput(4);
              color: theme.palette.normal.foregroundText;
            }
            Text { text: "K"; color: theme.palette.normal.foregroundText;}

            Text { text: i18n.tr("buffer memory");color: theme.palette.normal.foregroundText;}
            Text {
              id: buffermem;
              text: parseVmstatOutput(5);
              color: theme.palette.normal.foregroundText;
            }
            Text { text: "K"; color: theme.palette.normal.foregroundText;}

            Text { text: i18n.tr("swap cache");color: theme.palette.normal.foregroundText;}
            Text {
              id: swapcache;
              text: parseVmstatOutput(6);
              color: theme.palette.normal.foregroundText;
            }
            Text { text: "K"; color: theme.palette.normal.foregroundText;}

            Text { text: i18n.tr("total cache");color: theme.palette.normal.foregroundText;}
            Text {
              id: cachetotal;
              text: parseVmstatOutput(7);
              color: theme.palette.normal.foregroundText;
            }
            Text { text: "K"; color: theme.palette.normal.foregroundText;}

            Text { text: i18n.tr("used cache");color: theme.palette.normal.foregroundText;}
            Text {
              id: cacheused;
              text: parseVmstatOutput(8);
              color: theme.palette.normal.foregroundText;
            }
            Text { text: "K"; color: theme.palette.normal.foregroundText;}

            Text { text: i18n.tr("free cache");color: theme.palette.normal.foregroundText;}
            Text {
              id: cachefree;
              text: parseVmstatOutput(9);
              color: theme.palette.normal.foregroundText;
            }
            Text { text: "K"; color: theme.palette.normal.foregroundText;}

            Text { text: i18n.tr("active memory");color: theme.palette.normal.foregroundText;}
            Text {
              id: memactive;
              text: parseVmstatOutput(2);
              color: theme.palette.normal.foregroundText;
            }
            Text { text: "K"; color: theme.palette.normal.foregroundText;}

            Text { text: i18n.tr("inactive memory");color: theme.palette.normal.foregroundText;}
            Text {
              id: meminactive;
              text: parseVmstatOutput(3);
              color: theme.palette.normal.foregroundText;
            }
              Text { text: "K"; color: theme.palette.normal.foregroundText;}

            Text {
              anchors {
                  topMargin: units.gu(2)
              }
              text: "\n" + i18n.tr("How those values are related:");
              Layout.columnSpan: 3;
              font.bold: true;
              color: theme.palette.normal.baseText;
            }

            Text {
              Layout.column: 0;
              Layout.row: 13;
              text: "  " + i18n.tr("total memory");
              color: theme.palette.normal.foregroundText;
            }
            Text {
              Layout.column: 0;
              Layout.row: 14;
              text: "- " + i18n.tr("used memory");
              color: theme.palette.normal.foregroundText;
            }
            Text {
              Layout.column: 0;
              Layout.row: 15;
              text: "- " + i18n.tr("free memory");
              color: theme.palette.normal.foregroundText;
            }
            Text {
              Layout.column: 0;
              Layout.row: 16;
              text: "- " + i18n.tr("buffer memory");
              color: theme.palette.normal.foregroundText;
            }
            Text {
              Layout.column: 0;
              Layout.row: 17;
              text: "= " + i18n.tr("swap cache");
              color: theme.palette.normal.foregroundText;
            }

            Text {
              Layout.column: 0;
              Layout.row: 18;
              text: "\n" + "  " + i18n.tr("total cache");
              color: theme.palette.normal.foregroundText;
            }
            Text {
              Layout.column: 0;
              Layout.row: 19;
              text: "- " + i18n.tr("used cache");
              color: theme.palette.normal.foregroundText;
            }
            Text {
              Layout.column: 0;
              Layout.row: 20;
              text: "= " + i18n.tr("free cache");
              color: theme.palette.normal.foregroundText;
            }
        }
    }
}
