import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Shellexec 1.0

Page {
    title: i18n.tr("vnstat data usage statistics")

    header: PageHeader {
        id: header
        title: parent.title

        trailingActionBar.actions: [
            Action {
                iconName: "edit-clear"
                text: i18n.tr("Quit")
                onTriggered: {
                    Qt.quit()
                }
            },
            Action {
                iconName: "reload"
                text: i18n.tr("Reload data")
                onTriggered: {
                  texttodaydown.text = parseVnstatOutput(3).toString()
                  texttodayup.text = parseVnstatOutput(4).toString()
                  texttodaytotal.text = parseVnstatOutput(5).toString()
                  texttodayspeed.text = parseVnstatOutput(6).toString()
                  textcurrentmonthdown.text = parseVnstatOutput(8).toString()
                  textcurrentmonthup.text = parseVnstatOutput(9).toString()
                  textcurrentmonthtotal.text = parseVnstatOutput(10).toString()
                  textcurrentmonthspeed.text = parseVnstatOutput(11).toString()
                  textlastmonthdown.text = parseVnstatOutput(12).toString()
                  textlastmonthup.text = parseVnstatOutput(13).toString()
                  textlastmonthtotal.text = parseVnstatOutput(14).toString()
                }
            }
        ]
    }

    Shellexec {
      id: launchervnstat
    }

    function parseVnstatOutput(lineIndex) {
      //read output of command 'vnstat --online'
      var output = launchervnstat.launch("vnstat --oneline").split(";");
      var value = output[lineIndex];
      var bothMonths
      var thisMonth
      var lastMonth
      var bothMonthsUnit
      var thisMonthUnit
      var lastMonthUnit
      if (lineIndex > 11) {
        //calculate the total of last month from the diff of both months minus current month
        bothMonths = output[lineIndex].split(" ")[0].replace(",",".");
        bothMonthsUnit= output[lineIndex].split(" ")[1].substr(0,3);
        thisMonth = output[lineIndex-4].split(" ")[0].replace(",",".");
        thisMonthUnit= output[lineIndex-4].split(" ")[1].substr(0,3);
        if (bothMonthsUnit == thisMonthUnit) {
          lastMonthUnit = thisMonthUnit;
        }
        if (bothMonthsUnit === "GiB" && thisMonthUnit === "MiB"){
          lastMonthUnit = bothMonthsUnit;
          thisMonth = thisMonth/1000;
        }
        if (bothMonthsUnit.toString() === "GiB" && thisMonthUnit.toString() === "KiB"){
          lastMonthUnit = bothMonthsUnit;
          thisMonth = thisMonth/1000000;
        }
        lastMonth = Math.round((bothMonths-thisMonth)*100)/100;
        value = lastMonth.toString().replace(".",Qt.locale(Qt.locale().name).decimalPoint) + " " + lastMonthUnit;
      }
      return value.replace("\n","")
      //[0] number of ?
      //[1] interface
      //[2] date of last update
      //[3] today download
      //[4] today upload
      //[5] today total
      //[6] today rate
      //[7] current month's name
      //[8] current month's download
      //[9] current month's upload
      //[10] current month's total
      //[11] current month's rate
      //[12] current month + last month download
      //[13] current month + last month upload
      //[14] current month + last month total
    }

    Flickable {
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        contentHeight: vnstatgrid.height + units.gu(4)

        GridLayout {
            id: vnstatgrid
            columns: 2

            anchors {
              left: parent.left;
              top: parent.top;
              right: parent.right;
              margins: units.gu(2)
              }

              Text {
                text: i18n.tr("Detailed values of") + " \'vnstat\' :";
                Layout.columnSpan: 2;
                font.bold: true;
                color: theme.palette.normal.baseText;
              }

              Text { text: i18n.tr("interface: ");color: theme.palette.normal.foregroundText;}
              Text { text: parseVnstatOutput(1);color: theme.palette.normal.foregroundText;anchors.right: parent.right}

              Text { text: i18n.tr("data last updated") + ": ";color: theme.palette.normal.foregroundText;}
              Text { text: parseVnstatOutput(2);color: theme.palette.normal.foregroundText;anchors.right: parent.right}

              //divider
              Rectangle {
              height: 1
              color: theme.palette.normal.foregroundText
              Layout.columnSpan: 2
              anchors {
                  left: parent.left
                  right: parent.right
                  bottomMargin: units.gu(1)
                  bottom: texttodaydown.top
                  }
              }

              Text { text: i18n.tr("today download") + ": ";color: theme.palette.normal.foregroundText;}
              Text {
                id: texttodaydown;
                text: parseVnstatOutput(3).toString();
                color: theme.palette.normal.foregroundText;
                anchors.right: parent.right
              }

              Text { text: i18n.tr("today upload") + ": ";color: theme.palette.normal.foregroundText;}
              Text {
                id: texttodayup;
                text: parseVnstatOutput(4).toString();
                color: theme.palette.normal.foregroundText;
                anchors.right: parent.right
              }

              Text { text: i18n.tr("today total") + ": ";color: theme.palette.normal.foregroundText;}
              Text {
                id: texttodaytotal;
                text: parseVnstatOutput(5).toString();
                color: theme.palette.normal.foregroundText;
                anchors.right: parent.right
              }

              Text { text: i18n.tr("today avg. transfer rate") + ": ";color: theme.palette.normal.foregroundText;}
              Text {
                id: texttodayspeed;
                text: parseVnstatOutput(6).toString();
                color: theme.palette.normal.foregroundText;
                anchors.right: parent.right
              }

              //divider
              Rectangle {
              height: 1
              color: theme.palette.normal.foregroundText
              Layout.columnSpan: 2
              anchors {
                  left: parent.left
                  right: parent.right
                  topMargin: units.gu(1)
                  top: texttodayspeed.bottom
                  }
              }

              Text { text: i18n.tr("this month download") + ": ";color: theme.palette.normal.foregroundText;}
              Text {
                id: textcurrentmonthdown;
                text: parseVnstatOutput(8).toString();
                color: theme.palette.normal.foregroundText;
                anchors.right: parent.right
              }

              Text { text: i18n.tr("this month upload") + ": ";color: theme.palette.normal.foregroundText;}
              Text {
                id: textcurrentmonthup;
                text: parseVnstatOutput(9).toString();
                color: theme.palette.normal.foregroundText;
                anchors.right: parent.right
              }

              Text { text: i18n.tr("this month total") + ": ";color: theme.palette.normal.foregroundText;}
              Text {
                id: textcurrentmonthtotal;
                text: parseVnstatOutput(10).toString();
                color: theme.palette.normal.foregroundText;
                anchors.right: parent.right
              }

              Text { text: i18n.tr("this month avg. transfer rate") + ": ";color: theme.palette.normal.foregroundText;}
              Text {
                id: textcurrentmonthspeed;
                text: parseVnstatOutput(11).toString();
                color: theme.palette.normal.foregroundText;
                anchors.right: parent.right
              }

              //divider
              Rectangle {
              height: 1
              color: theme.palette.normal.foregroundText
              Layout.columnSpan: 2
              anchors {
                  left: parent.left
                  right: parent.right
                  topMargin: units.gu(1)
                  top: textcurrentmonthspeed.bottom
                  }
              }

              Text { text: i18n.tr("last month download") + ": ";color: theme.palette.normal.foregroundText;}
              Text {
                id: textlastmonthdown;
                text: parseVnstatOutput(12).toString();
                color: theme.palette.normal.foregroundText;
                anchors.right: parent.right
              }

              Text { text: i18n.tr("last month upload") + ": ";color: theme.palette.normal.foregroundText;}
              Text {
                id: textlastmonthup;
                text: parseVnstatOutput(13).toString();
                color: theme.palette.normal.foregroundText;
                anchors.right: parent.right
              }

              Text { text: i18n.tr("last month total") + ": ";color: theme.palette.normal.foregroundText;}
              Text {
                id: textlastmonthtotal;
                text: parseVnstatOutput(14).toString();
                color: theme.palette.normal.foregroundText;
                anchors.right: parent.right
              }
            }
    }
}
