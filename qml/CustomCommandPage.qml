import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Shellexec 1.0

Page {
  title: i18n.tr("run custom commands")

  header: PageHeader {
      id: header
      title: parent.title

      trailingActionBar.actions: [
        Action {
            iconName: "edit-clear"
            text: i18n.tr("Quit")
            onTriggered: {
                Qt.quit()
            }
        },
        Action {
            iconName: "reset"
            text: i18n.tr("Clear output")
            onTriggered: {
              outputField.text = i18n.tr("console output will appear here");
              command_input_field.text = "";
              }
            }
      ]
  }

    Shellexec {
      id: launcher
    }

    ColumnLayout {
      id: buttonColumn
      anchors {
        left: parent.left
        top: header.bottom
        right: parent.right
        margins: units.gu(2)
      }
      spacing: units.gu(1)

      Label {
        id: descriptiontext
        visible: true
        text: i18n.tr("Execute a custom command:")
        Layout.fillWidth: true
      }

      RowLayout {
        id: row_layout

        TextField {
          id: command_input_field

          width: parent.width - units.gu(2)
          anchors {
            topMargin: units.gu(2)
            horizontalCenter: parent.horizontalleft
          }
          text: ""
          placeholderText: i18n.tr("enter command here")
          inputMethodHints: Qt.ImhNoAutoUppercase
          onAccepted: {
            outputField.text = i18n.tr("command: ") + text
          }
        }

        Button {
          id: execButton
          height: units.gu(5)
          width: units.gu(15)
          anchors {
            topMargin: units.gu(2)
            leftMargin: units.gu(1)
            horizontalCenter: parent.horizontalleft
          }
          text: i18n.tr("execute")
          onTriggered: {
            outputField.text = "",
            outputField.text = launcher.launch(command_input_field.text),
            console.log("execute result: \n" + launcher.launch(command_input_field.text))
          }
        }
      }
   }

    Flickable {
      id: flickarea
      anchors {
        top: buttonColumn.bottom
        left: parent.left
        right: parent.right
        bottom: parent.bottom
        margins: units.gu(2)
      }

      clip: true
      contentHeight: outputField.height + units.gu(4)

      Label {
        id: outputField
        width: parent.width - units.gu(2)
        anchors {
          top: parent.top
          horizontalCenter: parent.horizontalleft
        }
        text: i18n.tr("console output will appear here")
        wrapMode: Text.WordWrap
      }
    }

}
