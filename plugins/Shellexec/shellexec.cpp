#include <QDebug>
#include <QTextStream>
#include <QProcess>
#include "shellexec.h"

Shellexec::Shellexec(QObject *parent) :
    QObject(parent),
    m_process(new QProcess(this))
{
}

QString Shellexec::launch(const QString program)
{
    QString output;
    try {
      m_process->start(program);
      m_process->waitForFinished(-1);
      output = m_process->readAllStandardOutput();
      //qDebug()<< "return value: " << output;
    } catch (QString fehler) {
      output = fehler;
      qDebug() << "Shellexec launch error:" << output;
    }

    return output;
}
